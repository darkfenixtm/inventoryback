package middlewares

type InId struct {
	Id int
}
type PurchaseBill struct {
	Id         int
	SupplierId int
	UserId     uint
	Total      float32
}
type SaleBill struct {
	Id          int
	Name        string
	Phone       string
	Direction   string
	IsDelivered bool
	IsPaid      bool
	UserId      uint
	Total       float32
}
