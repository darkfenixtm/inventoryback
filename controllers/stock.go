package controllers

import (
	"inventory/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetStockList(c *gin.Context) {
	var inJson models.Stock

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, models.GetStockList(inJson.Name))
}

func GetStock(c *gin.Context) {

	in := c.Query("Id")
	id, _ := strconv.Atoi(in)

	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en ingreso"})
		return
	}

	c.JSON(http.StatusOK, models.GetStock(id))
}

func CreateStock(c *gin.Context) {
	var inJson models.Stock

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Name == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en la creacion"})
		return
	}
	id := models.CreateStock(inJson)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en la creacion"})
		return
	}
	c.JSON(http.StatusOK, id)
}

func DeleteStock(c *gin.Context) {
	var inJson models.Stock

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	stock := models.DeleteStock(inJson.Id)
	//fmt.Println(stock)
	if !stock.IsDeleted {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error delete"})
		return
	}
	c.JSON(http.StatusOK, stock)

}
