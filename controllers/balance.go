package controllers

import (
	"github.com/gin-gonic/gin"
	"inventory/models"
	"net/http"
)

func GetBalance(c *gin.Context) {
	res, _ := models.GetBalance()
	if res.Id == 0 {
		id, err := models.CreateBalance(models.Balance{Balance: 0})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}
		res = models.Balance{Id: id, Balance: 0}
	}
	c.JSON(http.StatusOK, res)
}
