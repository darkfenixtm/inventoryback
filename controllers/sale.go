package controllers

import (
	"inventory/middlewares"
	"inventory/models"
	"inventory/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetSaleBill(c *gin.Context) {
	//	userId, _ := utils.ExtractTokenID(c)

	resIn, _ := models.GetSaleBill(0)
	var res []middlewares.SaleBill
	var total float32
	for _, s := range resIn {
		total = 0
		items, _ := models.GetSaleItem(s.Id)
		for _, item := range items {
			total += item.TotalPrice
		}
		var re middlewares.SaleBill
		re.Id = s.Id
		re.Name = s.Name
		re.Phone = s.Phone
		re.Direction = s.Direction
		re.IsDelivered = s.IsDelivered
		re.IsPaid = s.IsPaid
		re.UserId = s.UserId
		re.Total = total
		res = append(res, re)
	}

	c.JSON(http.StatusOK, res)
}

func CreateSaleBill(c *gin.Context) {
	userId, _ := utils.ExtractTokenID(c)

	var inJson models.SaleBill

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Name == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Datos faltantos"})
		return
	}
	inJson.UserId = userId
	id, _ := models.CreateSaleBill(inJson, userId)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en la creacion"})
		return
	}
	c.JSON(http.StatusOK, models.SaleBill{
		Id:          id,
		Name:        inJson.Name,
		Phone:       inJson.Phone,
		Direction:   inJson.Direction,
		IsDelivered: inJson.IsDelivered,
		IsPaid:      inJson.IsPaid,
	})
}

func UpdateSaleBill(c *gin.Context) {
	userId, _ := utils.ExtractTokenID(c)
	var inJson models.SaleBill
	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en parametros entrada"})
		return
	}
	id, _ := models.UpdateSaleBill(inJson, userId)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en la creacion"})
		return
	}
	c.JSON(http.StatusOK, models.SaleBill{
		Id: id,
	})
}

func DeleteSaleBill(c *gin.Context) {
	userId, _ := utils.ExtractTokenID(c)
	var inJson models.SaleBill

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error datos incompletos"})
		return
	}

	items, _ := models.GetSaleItem(inJson.Id)
	if len(items) > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "La factura contiene stock"})
		return
	}

	saleBill, _ := models.DeleteSaleBill(inJson.Id, userId)
	if saleBill.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en la creacion"})
		return
	}
	c.JSON(http.StatusOK, saleBill)
}

func GetSaleItem(c *gin.Context) {

	in := c.Query("SaleBillId")
	id, err := strconv.Atoi(in)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "En data de entrada"})
		return
	}
	res, _ := models.GetSaleItem(id)
	c.JSON(http.StatusOK, res)
}

func CreateSaleItem(c *gin.Context) {

	var inJson models.SaleItem

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, _ := models.CreateSaleItem(inJson)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}
	Stock := models.UpdateStock(inJson.StockId, -inJson.Quantity)
	if Stock.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Actualizando Stock"})
		return
	}

	balance, _ := models.UpdateBalance(inJson.TotalPrice)
	if balance.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}

	c.JSON(http.StatusOK, id)

}

func DeleteSaleItem(c *gin.Context) {
	var inJson models.SaleItem

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en parametro entrada"})
		return
	}
	SaleItem, _ := models.DeleteSaleItem(inJson.Id)
	if SaleItem.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}
	Stock := models.UpdateStock(SaleItem.StockId, SaleItem.Quantity)
	if Stock.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Actualizando Stock"})
		return
	}

	balance, _ := models.UpdateBalance(-inJson.TotalPrice)
	if balance.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}

	c.JSON(http.StatusOK, SaleItem)

}
