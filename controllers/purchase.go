package controllers

import (
	"inventory/middlewares"
	"inventory/models"
	"inventory/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetPurchaseBill(c *gin.Context) {
	//userId, _ := utils.ExtractTokenID(c)

	resIn, _ := models.GetPurchaseBill(0)
	var total float32
	var res []middlewares.PurchaseBill
	for _, s := range resIn {
		total = 0
		items, _ := models.GetPurchaseItem(s.Id)
		for _, item := range items {
			total += item.TotalPrice
		}
		var re middlewares.PurchaseBill
		re.Id = s.Id
		re.SupplierId = s.SupplierId
		re.UserId = s.UserId
		re.Total = total
		res = append(res, re)
	}

	c.JSON(http.StatusOK, res)
}

func CreatePurchaseBill(c *gin.Context) {
	userId, _ := utils.ExtractTokenID(c)
	var inJson models.PurchaseBill

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.SupplierId == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en parametros entrada"})
		return
	}
	id, _ := models.CreatePurchaseBill(inJson, userId)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en la creacion"})
		return
	}
	c.JSON(http.StatusOK, models.PurchaseBill{
		Id:         id,
		SupplierId: inJson.SupplierId,
	})
}

func DeletePurchaseBill(c *gin.Context) {
	userId, _ := utils.ExtractTokenID(c)
	var inJson models.PurchaseBill

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if inJson.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en parametros entrada"})
		return
	}

	items, _ := models.GetPurchaseItem(inJson.Id)
	if len(items) > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "La factura contiene stock"})
		return
	}

	purchaseBill, err := models.DeletePurchaseBill(inJson.Id, userId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, purchaseBill)
}

func GetPurchaseItem(c *gin.Context) {

	in := c.Query("PurchaseBillId")
	id, _ := strconv.Atoi(in)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error en parametros entrada"})
		return
	}
	purchaseItem, _ := models.GetPurchaseItem(id)
	c.JSON(http.StatusOK, purchaseItem)
}

func CreatePurchaseItem(c *gin.Context) {
	var inJson models.PurchaseItem

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, _ := models.CreatePurchaseItem(inJson)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}

	Stock := models.UpdateStock(inJson.StockId, inJson.Quantity)
	if Stock.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Actualizando Stock"})
		return
	}

	balance, _ := models.UpdateBalance(-inJson.TotalPrice)
	if balance.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}
	c.JSON(http.StatusOK, id)
}

func DeletePurchaseItem(c *gin.Context) {
	var inJson models.PurchaseItem

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	PurchaseItem, _ := models.DeletePurchaseItem(inJson.Id)
	if PurchaseItem.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}
	number := -PurchaseItem.Quantity

	Stock := models.UpdateStock(PurchaseItem.StockId, number)
	if Stock.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Actualizando Stock"})
		return
	}

	balance, _ := models.UpdateBalance(inJson.TotalPrice)
	if balance.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Creando item"})
		return
	}

	c.JSON(http.StatusOK, PurchaseItem)

}
