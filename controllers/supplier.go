package controllers

import (
	"inventory/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetSupplierList(c *gin.Context) {

	c.JSON(http.StatusOK, models.GetSupplierList())
}

func GetSupplier(c *gin.Context) {
	in := c.Query("Id")
	id, _ := strconv.Atoi(in)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error entrada"})
		return
	}
	c.JSON(http.StatusOK, models.GetSupplier(id))
}

func CreateSupplier(c *gin.Context) {
	var inJson models.Supplier

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Name == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error entrada"})
		return
	}
	id := models.CreateSupplier(inJson)
	if id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error creacion"})
		return
	}
	c.JSON(http.StatusOK, id)
}

func DeleteSupplier(c *gin.Context) {
	var inJson models.Supplier

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if inJson.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error entrada"})
		return
	}
	supplier := models.DeleteSupplier(inJson.Id)
	if supplier.Id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error delete"})
		return
	}
	c.JSON(http.StatusOK, supplier)
}
func UpdateSupplier(c *gin.Context) {
	var inJson models.Supplier

	if err := c.ShouldBindJSON(&inJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	supplier := models.UpdateSupplier(inJson.Id, inJson.Name, inJson.Phone)
	if len(supplier) <= 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error actualizar"})
		return
	}
	c.JSON(http.StatusOK, supplier)
}
