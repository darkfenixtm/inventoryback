package models

import (
	"gorm.io/gorm"
	"time"
)

type Stock struct {
	Id        int `gorm:"primaryKey;autoIncrement:true;unique"`
	Name      string
	Quantity  int  `gorm:"default:0"`
	IsDeleted bool `gorm:"default:false"`
}

type Supplier struct {
	Id        int `gorm:"primaryKey;autoIncrement:true;unique"`
	Name      string
	Phone     string
	IsDeleted bool `gorm:"default:false"`
}

type PurchaseBill struct {
	Id         int `gorm:"primaryKey;autoIncrement:true;unique"`
	SupplierId int
	UserId     uint
	Date       time.Time
	IsDeleted  bool `gorm:"default:false"`
}

type PurchaseItem struct {
	Id             int `gorm:"primaryKey;autoIncrement:true;unique"`
	PurchaseBillId int
	StockId        int
	Quantity       int
	PerPrice       float32
	TotalPrice     float32
}

type SaleBill struct {
	Id          int `gorm:"primaryKey;autoIncrement:true;unique"`
	Name        string
	Phone       string
	Direction   string
	IsDelivered bool `gorm:"default:false"`
	IsPaid      bool `gorm:"default:false"`
	UserId      uint
	Date        time.Time
	IsDeleted   bool `gorm:"default:false"`
}

type SaleItem struct {
	Id         int `gorm:"primaryKey;autoIncrement:true;unique"`
	SaleBillId int
	StockId    int
	Quantity   int
	PerPrice   float32
	TotalPrice float32
}

type Balance struct {
	Id      int `gorm:"primaryKey;autoIncrement:true;unique"`
	Balance float32
}

type User struct {
	Username string `gorm:"size:255;not null;unique"`
	Password string `gorm:"size:255;not null;"`
	gorm.Model
}
