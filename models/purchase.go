package models

func GetPurchaseBill(userId uint) (purchaseBill []PurchaseBill, err error) {

	if userId != 0 {
		result := DB.Where("user_id = ? and is_deleted = ?", userId, false).Find(&purchaseBill)
		if result.Error != nil {
			return nil, result.Error
		}
	} else {
		result := DB.Where("is_deleted = ?", false).Find(&purchaseBill)
		if result.Error != nil {
			return nil, result.Error
		}
	}

	return purchaseBill, nil

}

func CreatePurchaseBill(inJson PurchaseBill, userId uint) (id int, err error) {
	inJson.UserId = userId
	result := DB.Create(&inJson)
	if result.Error != nil {
		return 0, result.Error
	}
	return inJson.Id, nil
}

func DeletePurchaseBill(id int, userId uint) (purchaseBill PurchaseBill, err error) {
	//result := DB.Where("id = ? and user_id = ?", id, userId).First(&purchaseBill)
	//if result.Error != nil {
	//	return purchaseBill, result.Error
	//}
	//result = DB.Delete(&purchaseBill)
	//if result.Error != nil {
	//	return purchaseBill, result.Error
	//}
	result := DB.Model(&purchaseBill).Where("id = ? and user_id = ?", id, userId).Update("is_deleted", true)
	if result.Error != nil {
		return purchaseBill, result.Error
	}
	return purchaseBill, nil
}

func GetPurchaseItem(id int) (purchaseItem []PurchaseItem, err error) {
	result := DB.Where("purchase_bill_id = ?", id).Find(&purchaseItem)
	if result.Error != nil {
		return purchaseItem, result.Error
	}
	return purchaseItem, nil
}

func CreatePurchaseItem(inJson PurchaseItem) (id int, err error) {
	result := DB.Create(&inJson)
	if result.Error != nil {
		return 0, result.Error
	}
	return inJson.Id, nil
}

func DeletePurchaseItem(id int) (purchaseItem PurchaseItem, err error) {
	result := DB.Where("id = ?", id).First(&purchaseItem)
	if result.Error != nil {
		return purchaseItem, result.Error
	}
	result = DB.Delete(&purchaseItem)
	if result.Error != nil {
		return purchaseItem, result.Error
	}
	return purchaseItem, nil
}
