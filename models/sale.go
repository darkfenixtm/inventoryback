package models

func GetSaleBill(userId uint) (saleBill []SaleBill, err error) {
	if userId != 0 {
		result := DB.Where("user_id = ? and is_deleted = ?", userId, false).Find(&saleBill)
		if result.Error != nil {
			return nil, result.Error
		}
	} else {
		result := DB.Where("is_deleted = ?", false).Find(&saleBill)
		if result.Error != nil {
			return nil, result.Error
		}
	}

	return saleBill, nil
}

func CreateSaleBill(sale SaleBill, userId uint) (id int, err error) {
	sale.UserId = userId
	result := DB.Create(&sale)
	if result.Error != nil {
		return 0, result.Error
	}
	return sale.Id, nil
}

func UpdateSaleBill(sale SaleBill, userId uint) (id int, err error) {
	sale.UserId = userId
	var sale2 = SaleBill{Id: sale.Id}

	result := DB.Where("id = ?", sale.Id).Find(&sale2)
	if result.Error != nil {
		return 0, result.Error
	}
	sale.UserId = sale2.UserId
	if sale2.Id != 0 {
		DB.Save(&sale)
	}

	return sale.Id, nil
}

func DeleteSaleBill(id int, userId uint) (saleBill SaleBill, err error) {
	//result := DB.Where("id = ? and user_id", id, userId).First(&saleBill)
	//if result.Error != nil {
	//	return saleBill, result.Error
	//}
	//result = DB.Delete(&saleBill)
	//if result.Error != nil {
	//	return saleBill, result.Error
	//}
	result := DB.Model(&saleBill).Where("id = ? and user_id", id, userId).Update("is_deleted", true)
	if result.Error != nil {
		return saleBill, result.Error
	}
	return saleBill, nil
}

func GetSaleItem(id int) (saleBill []SaleItem, err error) {
	result := DB.Where("sale_bill_id = ?", id).Find(&saleBill)
	if result.Error != nil {
		return saleBill, result.Error
	}
	return saleBill, nil
}

func CreateSaleItem(inJson SaleItem) (id int, err error) {

	result := DB.Create(&inJson)
	if result.Error != nil {
		return 0, result.Error
	}
	return inJson.Id, nil
}

func DeleteSaleItem(id int) (saleItem SaleItem, err error) {

	result := DB.Where("id = ?", id).First(&saleItem)
	if result.Error != nil {
		return saleItem, result.Error
	}
	result = DB.Delete(&saleItem)
	if result.Error != nil {
		return saleItem, result.Error
	}
	return saleItem, nil
}
