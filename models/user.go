package models

import (
	"html"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

func (u *User) SaveUser() (*User, error) {

	//var err error
	err := u.generatePass()
	if err != nil {
		return &User{}, err
	}
	err = DB.Create(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

func (u *User) generatePass() error {
	//turn password into hash
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)

	//remove spaces in username
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))

	return nil
}

func (u *User) PrepareGive() {
	u.Password = ""
}
