package models

func GetSupplierList() (supplier []Supplier) {
	DB.Where("is_deleted = false").Find(&supplier)

	return supplier
}

func GetSupplier(id int) (supplier Supplier) {
	DB.Where("is_deleted = false and id = ?", id).Find(&supplier)
	return supplier
}

func CreateSupplier(supplier Supplier) (id int) {
	DB.Create(&supplier)
	return supplier.Id
}

func DeleteSupplier(id int) (supplier Supplier) {

	DB.Where("id = ?", id).Delete(&supplier)

	return supplier
}
func UpdateSupplier(id int, name string, phone string) (supplier []Supplier) {
	DB.Model(&supplier).Where("id = ?", id).Updates(Supplier{Name: name, Phone: phone})

	return supplier
}
