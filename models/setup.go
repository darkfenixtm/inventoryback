package models

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

var DB *gorm.DB

func Connectmodels() {
	host := os.Getenv("DB_HOST")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")
	port := os.Getenv("DB_PORT")
	sslmode := os.Getenv("DB_SSLMODE")
	timeZone := os.Getenv("DB_TIMEZONE")
	//dsn := "host=localhost user=postgres password=root dbname=inventory port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	dsn := "host=" + host + " user=" + user + " password=" + password + " dbname=" + dbname + " port=" + port + " sslmode=" + sslmode + " TimeZone=" + timeZone
	models, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to models!")
	}
	err = models.AutoMigrate(User{}, Supplier{}, PurchaseBill{}, PurchaseItem{}, SaleBill{}, SaleItem{}, Stock{}, Supplier{}, Balance{})
	if err != nil {
		panic("Failed to connect to models!")
	}
	DB = models

}
