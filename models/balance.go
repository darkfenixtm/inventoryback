package models

func GetBalance() (balance Balance, err error) {
	result := DB.First(&balance)
	if result.Error != nil {
		return balance, result.Error
	}
	return balance, nil
}

func CreateBalance(balance Balance) (id int, err error) {
	result := DB.Create(&balance)
	if result.Error != nil {
		return 0, result.Error
	}
	return balance.Id, nil
}

func UpdateBalance(imp float32) (balance Balance, err error) {
	result := DB.First(&balance)
	if result.Error != nil {
		return balance, result.Error
	}
	balance.Balance += imp
	if balance.Id != 0 {
		DB.Save(&balance)
	}
	return balance, nil
}
