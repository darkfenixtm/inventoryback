package models

func GetStockList(name string) (stock []Stock) {
	if name == "" {
		DB.Where("is_deleted = ?", false).Find(&stock)

	} else {
		DB.Where("is_deleted = ? and name like ?", false, "%"+name+"%").Find(&stock)
	}
	return stock
}

func GetStock(id int) (stock Stock) {

	DB.Where("id = ?", id).Find(&stock)

	return stock
}

func CreateStock(inJson Stock) (id int) {
	DB.Create(&inJson)
	return inJson.Id
}

func UpdateStock(id int, quantity int) (stock Stock) {
	DB.Where("id = ?", id).Find(&stock)
	Quantity := stock.Quantity
	stock.Quantity = Quantity + quantity
	if stock.Id != 0 {
		DB.Save(&stock)
	}
	return stock
}

func DeleteStock(id int) (stock Stock) {
	DB.Model(&stock).Where("id = ? and is_deleted = ?", id, false).Update("is_deleted", true)
	return stock
}
