package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"inventory/controllers"
	"inventory/middlewares"
	"os"

	// "inventory/middlewares"
	"inventory/models"
	"time"

	"github.com/gin-contrib/cors"

	"github.com/gin-gonic/gin"
	"github.com/jung-kurt/gofpdf"
)

func pdf(c *gin.Context) {
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	pdf.SetFont("Arial", "B", 16)
	pdf.Cell(40, 10, "Hello, World!")

	err := pdf.OutputFileAndClose("hello.pdf")
	if err != nil {
		panic(err)
	}
}

func main() {
	// gin.SetMode(gin.ReleaseMode)
	// r := gin.New()
	err := godotenv.Load(".env")

	if err != nil {
		fmt.Print("Error loading .env file")
	}

	models.Connectmodels()
	GIN_MODE := os.Getenv("GIN_MODE")
	router := gin.Default()
	//
	// config := cors.Config()
	// config.AllowOrigins = []string{"*"}
	// config.AllowCredentials = true
	// config.AllowAllOrigins = true

	// config.AllowHeaders = []string{"Authorization", "Content-Type"}
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"DELETE", "GET", "POST"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			// return origin == "https://github.com"
			return true
		},
		MaxAge: 12 * time.Hour,
	}))
	// router.Use(cors.Default())

	// router.Use(middlewares.CORSMiddleware())

	// Api
	v1 := router.Group("/")
	// v1.Use(middlewares.CORSMiddleware())
	{
		v1.POST("login", controllers.Login)

	}

	api := router.Group("/api")

	api.Use(middlewares.JwtAuthMiddleware())
	{
		api.POST("register", controllers.Register)
		api.GET("user", controllers.CurrentUser)

		//Vendedor
		api.GET("supplierList", controllers.GetSupplierList)
		api.GET("supplier", controllers.GetSupplier)
		api.POST("supplier", controllers.CreateSupplier)
		api.DELETE("supplier", controllers.DeleteSupplier)
		api.PUT("supplier", controllers.UpdateSupplier)

		//Stock
		api.POST("stockList", controllers.GetStockList)
		api.GET("stock", controllers.GetStock)
		api.POST("stock", controllers.CreateStock)
		api.DELETE("stock", controllers.DeleteStock)

		//Compras
		api.GET("purchaseBill", controllers.GetPurchaseBill)
		api.POST("purchaseBill", controllers.CreatePurchaseBill)
		api.DELETE("purchaseBill", controllers.DeletePurchaseBill)

		//Compras Item
		api.GET("purchaseItem", controllers.GetPurchaseItem)
		api.POST("purchaseItem", controllers.CreatePurchaseItem)
		api.DELETE("purchaseItem", controllers.DeletePurchaseItem)

		//ventas
		api.GET("saleBill", controllers.GetSaleBill)
		api.POST("saleBill", controllers.CreateSaleBill)
		api.DELETE("saleBill", controllers.DeleteSaleBill)
		api.POST("saleBillUpd", controllers.UpdateSaleBill)

		//ventas item
		api.GET("saleItem", controllers.GetSaleItem)
		api.POST("saleItem", controllers.CreateSaleItem)
		api.DELETE("saleItem", controllers.DeleteSaleItem)

		//balance
		api.GET("balance", controllers.GetBalance)

		api.POST("PDF", pdf)

	}

	if GIN_MODE == "release" {
		gin.SetMode(gin.ReleaseMode)

	}

	port := os.Getenv("PORT")
	err = router.Run(":" + port)
	if err != nil {
		return
	}
}
